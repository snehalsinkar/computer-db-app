package PageObjects;
import methods.ReUse;
import org.openqa.selenium.WebElement;

public class LandingListPageObject {
	public static WebElement pageTitle(){
		return ReUse.locateElement(ReUse.driver, "xpath", "//a[contains(text(),'Computer')]");
	}
	public static WebElement searchInputField(){
		return ReUse.locateElement(ReUse.driver, "id", "searchbox");
	}
	public static WebElement filterByName() {
		return ReUse.locateElement(ReUse.driver, "id", "searchsubmit");
	}
	public static WebElement addNewComputer() {
		return ReUse.locateElement(ReUse.driver, "id", "add");
	}
	
	public static WebElement pagination() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//div[contains(@id,'pagination')]//li[contains(@class,'next')]");
	}
	public static WebElement recordList() {
		return ReUse.locateElement(ReUse.driver, "xpath", ".//section//h1");
	}
	public static WebElement displayedRecords() {
		// TODO Auto-generated method stub
		return ReUse.locateElement(ReUse.driver, "xpath", ".//table[@class='computers zebra-striped']//tbody");
	}
}
