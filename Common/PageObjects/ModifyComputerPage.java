package PageObjects;
import methods.ReUse;
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.WebElement;

public class ModifyComputerPage {

	public static WebElement name() {
		return ReUse.locateElement(ReUse.driver, "id", "name");

	}

	public static WebElement Create() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//div/input[contains(@value,'Create this computer')]");
	}
	public static WebElement SuccessMessage() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//div[contains(@class,'alert-message warning')]");
	}
	public static WebElement FirstCellOfRecord() {
		return ReUse.locateElement(ReUse.driver, "xpath", ".//table[@class='computers zebra-striped']//tbody//tr[1]/td[1]/a");
	}
	public static WebElement Save() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//div/input[contains(@value,'Save this computer')]");
	}
	public static WebElement Delete() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//form/input[contains(@value,'Delete this computer')]");
	}
	public static WebElement noRecordFound() {
		return ReUse.locateElement(ReUse.driver, "xpath", "//div/em[contains(text(),'Nothing to display')]");
	}
}
