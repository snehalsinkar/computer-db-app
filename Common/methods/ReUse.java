package methods;

//import cucumber.api.java.it.Date;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.NoSuchElementException;


import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.SystemUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.cucumber.listener.Reporter;

import PageObjects.LandingListPageObject;

import org.junit.Assert;

public class ReUse {
	public static String resDir = null;
	public static WebElement we;
	public static WebDriver driver;
	public ReUse() {
	}
	
	public static void createChromeDriver() {
		System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
		if(SystemUtils.IS_OS_MAC)
		{
		System.setProperty("webdriver.chrome.driver", "chromedriverMac");
		} else if(SystemUtils.IS_OS_WINDOWS) {
			System.setProperty("webdriver.chrome.driver", "chromedriverWindows.exe");
		}else if(SystemUtils.IS_OS_LINUX){
			System.setProperty("webdriver.chrome.driver", "chromedriverLinux");
		}
		driver = new ChromeDriver();
		//return driver;
	}
	public static void pageLoadEvent() {
		
		ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor)driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(ReUse.driver, 30);
        wait.until(pageLoadCondition); 
	}
	/**
	 * This method will capture the screen shots.
	 *
	 * @param fileName
	 *            the file name
	 * @param driver
	 *            the driver
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void takeScreenShot(String fileName, WebDriver driver) throws IOException {
		Date now = new Date();
		// WebDriver driver = new FirefoxDriver();
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy hh mm ss");
		String time = dateFormat.format(now);
		resDir = System.getProperty("user.dir") + "//output//" + " " + fileName +" " + time;
		File resFolder = new File(resDir);
		resFolder.mkdir();
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy
		// somewhere

		FileUtils.copyFile(scrFile, new File(resFolder + "//" + fileName));
	}

	/**
	 * This method will add the screen shots to the existing folder.
	 *
	 * @param fileName
	 *            the file name
	 * @param driver
	 *            the driver
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */

	public static void addScreenShot(String fileName, WebDriver driver) throws IOException {

		File resFolder = new File(resDir);
		String tempPath = resFolder.getPath();
		File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
		// Now you can do whatever you need to do with it, for example copy
		// somewhere

		FileUtils.copyFile(scrFile, new File(tempPath + "//" + fileName));

	}

	/**
	 * This method scroll the page to find the required element.
	 *
	 * @param path
	 *            the path
	 * @return
	 * @return Element
	 */

	public static void scrollItem(String path, WebDriver driver) {
		// JavascriptExecutor jse = (JavascriptExecutor) driver;
		 WebElement element = driver.findElement(By.xpath(path));
		// jse.executeScript("arguments[0].scrollIntoView(true);", element);

		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
		// return element;
	}

	/**
	 * This method will convert the created xml report to JSON format.
	 *
	 * @return void
	 * @throws JSONException
	 *             the JSON exception
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	public static void convertReportToJson() throws JSONException, IOException {

		int PRETTY_PRINT_INDENT_FACTOR = 4;
		File reportFile;
		reportFile = FileUtils
				.getFile(System.getProperty("user.dir") + "//" + "test-output" + "//" + "testng-results.xml");
		JSONObject xmlJSONObj = org.json.XML.toJSONObject(FileUtils.readFileToString(reportFile, "UTF-8"));

		String jsonPrettyStr = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
		System.out.println(jsonPrettyStr);

		try (FileWriter file = new FileWriter(
				System.getProperty("user.dir") + "//" + "test-output" + "//" + "testng-results.json")) {
			file.write(jsonPrettyStr);

		} catch (IOException e) {

		}
	}
	
	/* This is common method to locate DOM element.
	 * Parameters it takes are driver reference, Locater type eg. xpath/cssselector/classname/id etc and path
	 * Usage example call : myDynamicElement = ReUse.locateElement(this.driver,"xpath","//div//button/span[contains(text(),'Submit')]");
	 * 
	 * 
	 * 
	 * 
	 */
	public static WebElement locateElement(WebDriver driver, String ByLocator, String location) { 
		  
        long longWait =30; 
        switch(ByLocator.toLowerCase()) { 
  
                        case "id": 
                         
                        try { 
                         
                        we = (new WebDriverWait(driver, longWait )).until(ExpectedConditions.presenceOfElementLocated(By.id(location))); 
                         
                        }catch(ElementNotVisibleException eire) { 
                            Reporter.addStepLog("Element: " + location + ", Element no visible on a page - "+driver.getTitle());                             
                            Assert.fail(); 
                             break; 
                     
                        }catch(TimeoutException toe) { 
                            Reporter.addStepLog("Element: " + location + ", Time out exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                         
                        } 
                    catch(NoSuchElementException nsee) { 
                             
                         Reporter.addStepLog("Element: " + location + ", No such element available on a page - "+driver.getTitle()); 
                         //junit.framework.TestCase.fail(); 
                         Assert.fail(); 
                         break; 
                        }catch(StaleElementReferenceException sere) { 
                            Reporter.addStepLog("Element: " + location + ", Stale Element exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                        } 
                     
                    catch(Exception e) { 
                         
                        Reporter.addStepLog("Element: " + location + ",Unknown Exception occured - "+driver.getTitle()); 
                        Assert.fail(); 
                         break; 
                        } 
                    finally { 
                         
                        } 
                    break; 
                         
                        case "xpath": 
  
                    try { 
                     
                        we = (new WebDriverWait(driver, longWait)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(location))); 
                     
                        }catch(ElementNotVisibleException eire) { 
                            Reporter.addStepLog("Element: " + location + ", Element no visible on a page - "+driver.getTitle());                             
                            Assert.fail(); 
                             break; 
                     
                        }catch(TimeoutException toe) { 
                            Reporter.addStepLog("Element: " + location + ", Time out exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                         
                        } 
                    catch(NoSuchElementException nsee) { 
                             
                         Reporter.addStepLog("Element: " + location + ", No such element available on a page - "+driver.getTitle()); 
                         //junit.framework.TestCase.fail(); 
                         Assert.fail(); 
                         break; 
                        }catch(StaleElementReferenceException sere) { 
                            Reporter.addStepLog("Element: " + location + ", Stale Element exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                        } 
                     
                    catch(Exception e) { 
                         
                        Reporter.addStepLog("Element: " + location + ",Unknown Exception occured - "+driver.getTitle()); 
                        Assert.fail(); 
                         break; 
                        } 
                    finally { 
                         
                        } 
                    break; 
                     
                    case "classname": 
                     
                    try { 
                     
                        we = (new WebDriverWait(driver, longWait)).until(ExpectedConditions.presenceOfElementLocated(By.className(location))); 
                         
                        }catch(ElementNotVisibleException eire) { 
                            Reporter.addStepLog("Element: " + location + ", Element no visible on a page - "+driver.getTitle());                             
                            Assert.fail(); 
                             break; 
                     
                        }catch(TimeoutException toe) { 
                            Reporter.addStepLog("Element: " + location + ", Time out exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                         
                        } 
                    catch(NoSuchElementException nsee) { 
                             
                         Reporter.addStepLog("Element: " + location + ", No such element available on a page - "+driver.getTitle()); 
                         //junit.framework.TestCase.fail(); 
                         Assert.fail(); 
                         break; 
                        }catch(StaleElementReferenceException sere) { 
                            Reporter.addStepLog("Element: " + location + ", Stale Element exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                        } 
                     
                    catch(Exception e) { 
                         
                        Reporter.addStepLog("Element: " + location + ",Unknown Exception occured - "+driver.getTitle()); 
                        Assert.fail(); 
                         break; 
                        } 
                    finally { 
                         
                        } 
                    break; 
                     
                    case "name": 
                     
                    try { 
                     
                        we = (new WebDriverWait(driver, longWait)).until(ExpectedConditions.presenceOfElementLocated(By.tagName(location))); 
                         
                        }catch(ElementNotVisibleException eire) { 
                            Reporter.addStepLog("Element: " + location + ", Element no visible on a page - "+driver.getTitle());                             
                            Assert.fail(); 
                             break; 
                     
                        }catch(TimeoutException toe) { 
                            Reporter.addStepLog("Element: " + location + ", Time out exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                         
                        } 
                    catch(NoSuchElementException nsee) { 
                             
                         Reporter.addStepLog("Element: " + location + ", No such element available on a page - "+driver.getTitle()); 
                         //junit.framework.TestCase.fail(); 
                         Assert.fail(); 
                         break; 
                        }catch(StaleElementReferenceException sere) { 
                            Reporter.addStepLog("Element: " + location + ", Stale Element exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                        } 
                     
                    catch(Exception e) { 
                         
                        Reporter.addStepLog("Element: " + location + ",Unknown Exception occured - "+driver.getTitle()); 
                        Assert.fail(); 
                         break; 
                        } 
                    finally { 
                         
                        } 
                    break; 
                     
                    case "css": 
                     
                    try { 
                     
                        we = (new WebDriverWait(driver, longWait)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(location))); 
                         
                        }catch(ElementNotVisibleException eire) { 
                            Reporter.addStepLog("Element: " + location + ", Element no visible on a page - "+driver.getTitle());                             
                            Assert.fail(); 
                             break; 
                     
                        }catch(TimeoutException toe) { 
                            Reporter.addStepLog("Element: " + location + ", Time out exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                         
                        } 
                    catch(NoSuchElementException nsee) { 
                             
                         Reporter.addStepLog("Element: " + location + ", No such element available on a page - "+driver.getTitle()); 
                         //junit.framework.TestCase.fail(); 
                         Assert.fail(); 
                         break; 
                        }catch(StaleElementReferenceException sere) { 
                            Reporter.addStepLog("Element: " + location + ", Stale Element exception - "+driver.getTitle()); 
                            Assert.fail(); 
                             break; 
                        } 
                     
                    catch(Exception e) { 
                         
                        Reporter.addStepLog("Element: " + location + ",Unknown Exception occured - "+driver.getTitle()); 
                        Assert.fail(); 
                         break; 
                        } 
                    finally { 
                         
                        } 
                    break; 
                    } 
                     
                return(we); 
                 
                } 

}
