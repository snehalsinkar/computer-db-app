Feature: Verify computer DB web portal list page functionality 

Scenario: create environment for test
Given a common driver variable is created
 
  
Scenario: verify landing page 
    Given user navigates to the given URL 
      | Fields   | Values                               | 
      | Url      | http://computer-database.herokuapp.com/computers | 
       
      When page loads completely 
      Then verify landing page title
      And verify presence of search box
      And verify presence of search submit button
      And verify presence of add new computer button
      And verify presence of next button
  