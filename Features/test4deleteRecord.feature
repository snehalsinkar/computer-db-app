Feature: Verify computer DB web portal delete record functionality 
  Scenario: verify delete record 
    Given user serch a record 
      | Fields   | Values                               | 
      |computerName|ASTON| 
       
      When user delete a record 
      Then delete success UI message is displayed
      And search same record returns no result
  
       
     And close the driver 