# Computer-database

Automation test project for regression testing of "Computer database sample application".

Application location: https://computer-database.herokuapp.com/

## Getting Started
1.Java 8 + Maven + Cucumber + Selenium +JUnit .
2.Extent Reports are used for reporting purpose which will reside in ./output/Reports
3.By a default suite will be executed Chrome browser(chromedriver provided in project resources)

### Prerequisites
Maven
## Running the tests & things you need to install the software and how to install them

1. To run full test suite: 1 right click on pom.xml file > RunAs>> mvn clean install
2. Right click on ./RunTest/runner/RunTest.java >> RunAs >>JUnit Test

### For Report

After Test Run refresh ./output
Inside ./output/Report/Test_Report.html will be generated


## Enhancements ToDO

* Cross Browser 
* separate Test Data in external file
* etc
