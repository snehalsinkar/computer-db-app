package runner;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

import org.junit.runner.RunWith;

@RunWith(Cucumber.class) 
@CucumberOptions(plugin = {"com.cucumber.listener.ExtentCucumberFormatter:output/Report/Test_Report.html"}, features = {"Features/test1computerDBListPage.feature" ,"Features/test2addComputer.feature","Features/test3updateRecord.feature","Features/test4deleteRecord.feature"}, glue = { "webPortal"})

public class RunTest {

	
}
