package webPortal;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import PageObjects.LandingListPageObject;
import PageObjects.ModifyComputerPage;
import methods.ReUse;
public class EditComputerTest {
	@Given("^user search a record by name$")
	public void user_search_a_record_by_name(DataTable arg1) throws Throwable {
		List<List<String>> data = arg1.raw();
		ReUse.we=LandingListPageObject.searchInputField();
		ReUse.we.clear();
	    ReUse.we.sendKeys(data.get(1).get(1));
	    ReUse.addScreenShot("EditRecord", ReUse.driver);
	    ReUse.takeScreenShot("recordSearchString", ReUse.driver);
	    LandingListPageObject.filterByName().click();
	    ReUse.pageLoadEvent();
	    ReUse.takeScreenShot("recordList", ReUse.driver);
	    ModifyComputerPage.FirstCellOfRecord().click();
	    ReUse.pageLoadEvent();
	    ReUse.takeScreenShot("RecordBeingEdited", ReUse.driver);
	    ModifyComputerPage.name().sendKeys(data.get(2).get(1));
	    ReUse.takeScreenShot("EditedRecordTitle", ReUse.driver);
	   
	   	}

	@When("^save$")
	public void save() throws Throwable {
		 ModifyComputerPage.Save().click();
		    ReUse.pageLoadEvent();
		    ReUse.takeScreenShot("EditSuccessMessage", ReUse.driver);
		    
	    	}

	@Then("^success message$")
	public void success_message() throws Throwable {
		ReUse.we =  ModifyComputerPage.SuccessMessage();
		   assertThat(ReUse.we.isDisplayed(), is(true));

	    	}
	
	
}
