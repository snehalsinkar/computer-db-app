package webPortal;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import methods.ReUse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import PageObjects.*
;public class DeleteRecord {
	public List<List<String>> data;
	@Given("^user serch a record$")
	public void user_serch_a_record(DataTable arg1) throws Throwable {
		data = arg1.raw();
		methods.ReUse.takeScreenShot("DeleteRecordPage",ReUse.driver);
		ReUse.we = LandingListPageObject.searchInputField();
		ReUse.we.clear();
	    ReUse.we.sendKeys(data.get(1).get(1));
	    LandingListPageObject.filterByName().click();
	    ReUse.pageLoadEvent();
	    ReUse.takeScreenShot("searchToDelete", ReUse.driver);
	}

	@When("^user delete a record$")
	public void user_delete_a_record() throws Throwable {
		ModifyComputerPage.FirstCellOfRecord().click();
		ReUse.pageLoadEvent();
		ReUse.takeScreenShot("RecordToBeDeleted", ReUse.driver);
		ModifyComputerPage.Delete().click();
	   	}

	@Then("^delete success UI message is displayed$")
	public void delete_success_UI_message_is_displayed() throws Throwable {
		ReUse.pageLoadEvent();
		ReUse.takeScreenShot("DeleteRecordSuccess", ReUse.driver);
		ReUse.we = ModifyComputerPage.SuccessMessage();
		assertThat(ReUse.we.isDisplayed(), is(true));

	    	}

	@Then("^search same record returns no result$")
	public void search_same_record_returns_no_result() throws Throwable {
		ReUse.we = LandingListPageObject.searchInputField();
		ReUse.we.clear();
		ReUse.we.sendKeys(data.get(1).get(1));
		LandingListPageObject.filterByName().click();
		ReUse.pageLoadEvent();
		ReUse.takeScreenShot("nothingToDisplay", ReUse.driver);
		ReUse.we = ModifyComputerPage.noRecordFound();
		assertThat(ReUse.we.isDisplayed(), is(true));

	  	}
	@Then("^close the driver$")
	public void close_the_driver() throws Throwable {
		ReUse.driver.quit();
	}

}
