package webPortal;
import org.openqa.selenium.JavascriptExecutor;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//import cucumber.api.java.gl.E;
import methods.ReUse;
import PageObjects.LandingListPageObject;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Test {
	
	@Given("^a common driver variable is created$")
	public void a_common_driver_variable_is_created() throws Throwable {
	    methods.ReUse.createChromeDriver();
	    methods.ReUse.takeScreenShot("LandingPage",ReUse.driver);

	}
	@Given("^user navigates to the given URL$")
	public void user_navigates_to_the_given_URL(DataTable arg1) throws Throwable {
		List<List<String>> data = arg1.raw();
		ReUse.driver.navigate().to(data.get(1).get(1));
		ReUse.driver.manage().window().maximize();
		
	}
	/* This function will verify if
	 * page is completely loaded and 
	 * if not it will wait.
	 * And upon load will take screenshot
	 * 
	 */
	@When("^page loads completely$")
	public void page_loads_completely() throws Throwable {
		ReUse.pageLoadEvent(); 
        /*
         * Following code snippet will give call 
         * to addScreenShot which will be created 
         * in output folder
         */
        ReUse.addScreenShot("ComputerListPageLoad", ReUse.driver);
    }
	   
		/*Following methods will verify presence of following elements on list page
		 * title
		 * search box
		 * submit button
		 * add new computer button
		 * pagination
		 */

	@Then("^verify landing page title$")
	public void verify_landing_page_title() throws Throwable {	   
	   	ReUse.we = LandingListPageObject.pageTitle();
 		assertThat(ReUse.we.isDisplayed(), is(true));
		
		/*
		 * Usage Example assertThat() 
		 * assertThat(objectUnderTest, is(not(someOtherObject)));
		 * assertThat(objectUnderTest, not(someOtherObject));
		 *	assertThat(objectUnderTest, not(equalTo(someOtherObject)));
		 */
	}
	@Then("^verify presence of search box$")
	public void verify_presence_of_search_box() throws Throwable {
		ReUse.we = LandingListPageObject.searchInputField();
 		assertThat(ReUse.we.isDisplayed(), is(true));
	   	}

	@Then("^verify presence of search submit button$")
	public void verify_presence_of_search_submit_button() throws Throwable {
		ReUse.we = LandingListPageObject.filterByName();
		assertThat(ReUse.we.isDisplayed(), is(true));
	    	}

	@Then("^verify presence of add new computer button$")
	public void verify_presence_of_add_new_computer_button() throws Throwable {
		ReUse.we = LandingListPageObject.addNewComputer();
		assertThat(ReUse.we.isDisplayed(), is(true));
	    	}

	@Then("^verify presence of next button$")
	public void verify_presence_of_next_button() throws Throwable {
		ReUse.we = LandingListPageObject.pagination();
		assertThat(ReUse.we.isDisplayed(), is(true));

	  	}
	
}
