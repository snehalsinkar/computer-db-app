package webPortal;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import methods.ReUse;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import PageObjects.ModifyComputerPage;
import PageObjects.LandingListPageObject;
public class AddComputerTest {
	public String computerName;
	@Given("^user add computer record with valid mandatory key values$")
	public void user_add_computer_record_with_valid_mandatory_key_values(DataTable arg1) throws Throwable {
		List<List<String>> data = arg1.raw();
		computerName = data.get(1).get(1);
		methods.ReUse.takeScreenShot("AddRecordPage",ReUse.driver);

		System.out.println("ComputerName:::"+computerName);
		LandingListPageObject.addNewComputer().click();
		ReUse.pageLoadEvent();      
        ReUse.addScreenShot("AddRecordPageLoad", ReUse.driver);
        ModifyComputerPage.name().sendKeys(computerName);
        ReUse.addScreenShot("NewRecordName", ReUse.driver);
	}

	@Given("^create a record$")
	public void create_a_record() throws Throwable {
		ModifyComputerPage.Create().click();
		ReUse.pageLoadEvent(); 
		ReUse.addScreenShot("SuccessNewRecord", ReUse.driver);
	    	}

	@When("^success$")
	public void success() throws Throwable {
	   ReUse.we =  ModifyComputerPage.SuccessMessage();
	   assertThat(ReUse.we.isDisplayed(), is(true));

	}

	@Then("^verify record by searching$")
	public void verify_record_by_searching() throws Throwable {
		LandingListPageObject.searchInputField().sendKeys(computerName);
		LandingListPageObject.filterByName().click();
		ReUse.addScreenShot("SearchResultRecord", ReUse.driver);

	    	}
	

}
